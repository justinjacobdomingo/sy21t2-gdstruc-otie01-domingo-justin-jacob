package Com.Quiz6;

public class node {
    private int data;
    private node rightChild;
    private node leftChild;
    private int leastValue;
    private int maxValue;

    public node(int _data){
        this.data = _data;
    }

    public void insert(int value){
        if(value ==  data){
            return;
        }
        if(value < data){
            if(leftChild == null){
                leftChild = new node((value));
            }
            else{
                leftChild.insert(value);
            }
        }
        else{
            if(rightChild == null){
                rightChild = new node(value);
            }
            else{
                rightChild.insert(value);
            }
        }
    }

    public void traverseInOrder(){
        if(leftChild != null){
            leftChild.traverseInOrder();
        }
        System.out.println("Data: " + data);

        if (rightChild != null) {
            rightChild.traverseInOrder();
        }
    }

    public void traverseInOrderDecending(){
        if (rightChild != null) {
            rightChild.traverseInOrderDecending();
        }
        System.out.println("Data: " + data);

        if(leftChild != null){
            leftChild.traverseInOrderDecending();
        }
    }

    public void getMin(){
        if(leftChild != null){
            leftChild.getMin();
        }
        leastValue = data;

        if(leftChild == null ){
            System.out.println("Least Value: " + leastValue);
            return;
        }
    }

    public void getMax(){
        if(rightChild != null){
            rightChild.getMax();
        }
        maxValue = data;
        if(rightChild == null ){
            System.out.println("Maxinum Value: " + maxValue);
            return;
        }
    }

    public  node get(int value){
        if(value == data){
            return this;
        }
        if(value < data){
            if(leftChild != null){
                return leftChild.get(value);
            }
        }
        else{
            if (rightChild != null) {
                return rightChild.get(value);
            }
        }
        return null;
    }

    //--------------------------------------
    @Override
    public String toString() {
        return "node{" +
                "data=" + data +
                '}';
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public node getRightChild() {
        return rightChild;
    }

    public void setRightChild(node rightChild) {
        this.rightChild = rightChild;
    }

    public node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(node leftChild) {
        this.leftChild = leftChild;
    }
}
