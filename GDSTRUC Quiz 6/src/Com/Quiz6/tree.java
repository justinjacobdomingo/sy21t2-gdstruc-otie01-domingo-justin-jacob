package Com.Quiz6;

public class tree {

    private node root;

    public void insert(int imput){
        if(root == null){
            root = new node(imput);
        }
        else{
            root.insert(imput);
        }
    }

    public void traverseInOrder(){
        if(root != null){
            root.traverseInOrder();
        }
    }

    public void traverseInOrderDecending(){
        if(root != null){
            root.traverseInOrderDecending();
        }
    }

    public node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    public void lowest(){
        if(root != null){
            root.getMin();
        }
    }

    public void maxinum(){
        if(root != null){
            root.getMax();
        }
    }
}
