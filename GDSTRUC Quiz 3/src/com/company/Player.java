package com.company;

import java.util.Objects;

public class Player {
    private int playerID;
    private String userName;
    private int level;

    public Player(int id, String name, int level) {
        this.playerID = id;
        this.userName = name;
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerID == player.playerID && level == player.level && userName.equals(player.userName);
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerID=" + playerID +
                ", userName='" + userName + '\'' +
                ", level=" + level +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerID, userName, level);
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
