package com.company;

public class Main {

    public static void main(String[] args) {

        ArrayQueue queue = new ArrayQueue(50);

        for(int i = 0; i != 10;i++) {
            System.out.println();
            System.out.println(" ----- game: " + (i + 1) + " ------- ");
            queue.countPlayer();

            System.out.println(" ----- player in queue:" + queue.getNumInQueue() +" ------- ");
            queue.printQueue();
            System.out.println();
        }
        System.out.println("game ended");
    }
}

