package com.company;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class ArrayQueue {
    private  Player[] queue;
    private int front;
    private int back;
    private int numInQueue = 0;

    public int getNumInQueue() {
        return numInQueue;
    }

    public ArrayQueue(int capacity){
        queue = new Player[capacity];
    }

    public void add(Player player){
        if(back == queue.length){
            Player[] newArray = new Player[queue.length * 2];
            System.arraycopy(queue,0, newArray,0, queue.length);
            queue = newArray;
        }// resize the array

        queue[back] = player;
        back++;
    }

    public Player remove() {
        if (getSize() == 0) {
            throw new NoSuchElementException();
        }

        Player removePlayer = queue[front];
        queue[front] = null;
        front++;

        if(getSize() == 0){
           front = 0;
           back = 0;
        } // reset trackers when queue is empty

        return removePlayer;
    }

    public Player peek(){
        if(getSize() == 0){
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public int getSize(){
        return back - front;
    }

    public void printQueue(){
        for(int i =front; i <back; i++){
            System.out.println(queue[i]);
        }

    }

    public void SystemPause(){
        Scanner pause = new Scanner(System.in);
        System.out.println("Press enter to continue....");
        String pauseGiven = pause.nextLine();

    }

    public int randTurns(){
        int randomNum = ((int)(Math.random() * 7 ) + 1);
        return randomNum;
    }

    public int randLevels(){
        int randomNum = ((int)(Math.random() * 100 ) + 1);
        return randomNum;
    }

    public void countPlayer(){
        while(numInQueue < 5){
            playersQueuing();
        }
        int count = 0;
        boolean statement = true;
        while(count != 5){
            if(queue[front] != null){
                System.out.println("Player that is entering the game: " + remove());
                count++;
                numInQueue -= 1;
            }

        }
    }

   public void playersQueuing(){
        int numberGiven = randTurns();
       Scanner inputGiven = new Scanner(System.in);

        System.out.println(numberGiven + " entering queue");
        System.out.println("input their userNames");

        for(int i = 0; i != numberGiven; i++){
            System.out.print(i + 1 +" :");
            String pauseGiven = inputGiven.nextLine();
            add(new Player(back +1, pauseGiven,randLevels()));
            numInQueue++;
        }
   }
}
