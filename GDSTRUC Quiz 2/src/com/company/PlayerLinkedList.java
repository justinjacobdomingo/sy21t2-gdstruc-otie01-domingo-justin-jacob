package com.company;

import org.w3c.dom.Node;

public class PlayerLinkedList {
    private PlayerNode head;

    public void addToFront (Player player){
        PlayerNode playerNode= new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;


    }

    public int printList(int given) {
        PlayerNode current = head;
        System.out.println("Head -> ");
        while(current != null){
            System.out.print("number " + (given + 1) + " : ");
            System.out.print(current);
            System.out.println(" -> ");
            current = current.getNextPlayer();
            given++;
        }

        System.out.println("Null");
        System.out.println("Count= " + given );
        return given;
    }

    public void removeFirstNode(){
        PlayerNode temp = new PlayerNode(null);
        temp = head;
        head = head.getNextPlayer();
        temp = null;

    }

    public void containsString(String given){
        PlayerNode current = head;
        boolean statement = false;
        while(current != null){
            if(current.getPlayer().toString().contains(given)){
                statement = true;
            }
            current = current.getNextPlayer();
        }

        System.out.println(statement);
    }
    // is this code oki?

    public void indexString(String player){
        PlayerNode current = head;
        int givenNum = 0;
        int temp = 0;
        while(current != null){
            if(current.getPlayer().toString().contains(player)) {
                givenNum = temp;
                System.out.println(givenNum);
            }
            current = current.getNextPlayer();
            temp++;
        }
    }
}
