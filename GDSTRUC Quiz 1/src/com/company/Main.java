package com.company;

import java.util.Scanner;

public class Main {

    private static int askNumArray(){
        Scanner sc = new Scanner(System.in);

        System.out.print("How many index do you want: ");
        int givenNum  = sc.nextInt();

        return givenNum;
    }

    private static void randomNumGen(int[] givenArray,  int[] tempArray ,int maxNum) {
        for(int i = 0 ; i <  givenArray.length; i++){
            // random number generator [1]
            int randNum = (int) (Math.random() * maxNum);
               givenArray[i] = randNum;
               tempArray[i] = randNum;
        }
    }

    private static void bubbleSortGen(int[] givenArray){
        for(int i = givenArray.length - 1; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++ ){
                if(givenArray[j] < givenArray[j+1]) {
                    int temp = givenArray[j];
                    givenArray[j] = givenArray[j + 1];
                    givenArray[j + 1] = temp;
                }
            }
       }
    }

    private static void swapArrayElements(int[] givenArray, int[] tempArray){
        for(int i = 0; i < givenArray.length; i++){
            givenArray[i] = tempArray[i];
        }
    }

    private static void descendingSelectionSortGen(int[] givenArray){
        for(int i = givenArray.length - 1; i > 0 ; i--){

            int smallestIndex = 0;

            for(int j = i ; j > 0 ; j-- ){
                if(givenArray[j] < givenArray[smallestIndex]) {
                    smallestIndex = j;
                }
            }
            int temp = givenArray[i];
            givenArray[i] = givenArray[smallestIndex];
            givenArray[smallestIndex] = temp;
        }
    }

    private static void printNum(int[] givenArrays){
        for(int i : givenArrays) {
            System.out.print( i + " ");
        }
    }

    public static void main(String[] args) {

        int a = askNumArray(); // ask how many index;
        int[] numbers = new int[a];
        int[] tempNumbers = new int[a];

        randomNumGen(numbers,tempNumbers,100);

        // bubble sorting
        System.out.println("\n------\nBubble sort(descending)");
        System.out.println("\nbefore sort:");
        printNum(numbers);

        System.out.println("\n\nafter sort (bubble sort):");
        bubbleSortGen(numbers);
        printNum(numbers);

        //selection sort descending
        System.out.println("\n------\nSelection sort (descending)");
        System.out.println("\nbefore sort:");
        swapArrayElements(numbers,tempNumbers);
        printNum(numbers);

        System.out.println("\n\nafter sort (Selection sort):");
        descendingSelectionSortGen(numbers);
        printNum(numbers);

    }
}

//reference
//https://www.educative.io/edpresso/how-to-generate-random-numbers-in-java  [1]
//https://stackoverflow.com/questions/4238384/java-equivalent-of-cin-c [2]
//https://www.programiz.com/java-programming/enhanced-for-loop [3]