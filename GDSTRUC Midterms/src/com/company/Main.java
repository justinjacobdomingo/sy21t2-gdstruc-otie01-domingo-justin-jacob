package com.company;



public class Main {

    public static void main(String[] args) {



        CardStack cardStack = new CardStack(30,"Stack");
        CardStack discardedStack = new CardStack(30,"Discarded Stack");
        CardStack cardStackOnHand = new CardStack(5,"Card on hand");

        cardStack.askName(cardStack);

        /*cardStack.pickCard(cardStackOnHand);

        System.out.println("------------Card on Hand---------------");
        cardStackOnHand.printCard();
        */

        while (cardStack.getCardOnHand() != 0)
        {

            cardStack.turnCards(discardedStack,cardStack,cardStackOnHand);


            System.out.println("how many card there is in the deck: "  + cardStack.getCardOnHand());
            System.out.println("how many card there is on hand: "  + cardStackOnHand.getCardOnHand());
            System.out.println("how many card there is discarded: "  + discardedStack.getCardOnHand());

            System.out.println("--------------------------------------------------------------------");

            cardStack.SystemPause();
        }


    }
}


