package com.company;

import java.util.EmptyStackException;

import java.util.Scanner;

public class CardStack {
    private Card[] stacks;
    private int top;
    private int randomNum = (int) Math.random() * 1 - 0;
    private int cardOnDeck = 0;
    private Scanner scanner = new Scanner(System.in);
    private String cardName;

    public int getCardOnHand() {
        return cardOnDeck;
    }

    public CardStack(int capacity,String Name) {
        stacks = new Card[capacity];
        cardName = Name;


    }

    // -------------------------------------------------------

    public void addName(Card given) {
        if (top == stacks.length) {
            Card[] newStack = new Card[2 * stacks.length];
            System.arraycopy(stacks, 0, newStack, 0, stacks.length);
            stacks = newStack;
        }

        cardOnDeck++;
        stacks[top++] = given;
    }

    public void askName(CardStack givenCardStack){

        for(int i = 0; i != 30; i++){

            System.out.println("Input Name: ");
            String givenString = scanner.nextLine();

            givenCardStack.addName(new Card(i+1,givenString));
        }
    }

    //gets name

    public void pickCard(CardStack transferCard){

        int randomGen = ((int) (Math.random() * 5) + 1);

        while(randomGen > getCardOnHand())
        {
            randomGen = ((int) (Math.random() * 5) + 1);
        }
        System.out.println("get " + randomGen + "x ");
        for (int i = 0; i < randomGen; i++) {
            transferCard(transferCard);
        }
    }

    public Card transferCard(CardStack givenStack) {

        boolean statement = true;

        int randomGenerator = (int) ((Math.random() * 5) + 1);


        while(statement) {
            int i = (int) (Math.random() * top);

            if (isEmpty()) {
                throw new EmptyStackException();
            }

            Card poppedPlayer = stacks[i];
            if (poppedPlayer != null) {
                givenStack.addName(poppedPlayer);
                stacks[i] = null;

                cardOnDeck--;
                System.out.println("Got: " + poppedPlayer);

                return poppedPlayer;
            }
        }
        return null;
    }

    public boolean isEmpty(){
        return top == 0;
    }

    // transfers card


    public void printCard(){
        System.out.println();
        for(int i = top -1; i >= 0; i--){
            if(stacks[i] != null) {
                System.out.println(stacks[i]);
            }
        }
        countCard();
    }
    // prints card

    public void countCard(){
        System.out.println("Size of " + cardName + ": " + cardOnDeck);
    }
    //count card

    public void turnCards(CardStack givenA, CardStack givenB, CardStack givenC){

        int randomTurn = ((int) (Math.random() * 3) + 1);
        //a = discard b = stack c = hand
        System.out.println();
        boolean turnBool = true;

        while(turnBool) {
            if (randomTurn == 3) {
                System.out.println("Player has to pick card from deck ");
                givenB.pickCard(givenC);
                turnBool = false;
            }
            else if (randomTurn == 2 && givenC.getCardOnHand() != 0) {
                System.out.println("Player has to discard card from hand ");
                givenC.pickCard(givenA);
                turnBool = false;
            }
            else if (randomTurn == 1 && givenA.getCardOnHand() != 0) {
                System.out.println("Player has to get card from discarded pile ");
                givenA.pickCard(givenC);
                turnBool = false;
            }
            randomTurn = ((int) (Math.random() * 3) + 1);
        }
        System.out.println("------------" + givenC.cardName + "---------------");
        givenC.printCard();
        System.out.println();
    }

    public void SystemPause(){
       Scanner pause = new Scanner(System.in);
        System.out.println("Press enter to continue....");
        String pauseGiven = pause.nextLine();

    }


}
