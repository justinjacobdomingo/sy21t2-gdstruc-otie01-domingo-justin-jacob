package com.company;

public class Main {

    public static void main(String[] args) {

        Player Qui = new Player(1,"Qui",10);
        Player Lester = new Player(2,"Lester",0);
        Player Berserker = new Player(3,"Berserker",100);
        Player Guys = new Player(4,"zaaa",10);
        Player Scam = new Player(5,"Dong",-1);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(Qui.getUserName(),Qui);
        hashtable.put(Lester.getUserName(),Lester);
        hashtable.put(Berserker.getUserName(),Berserker);
        hashtable.put(Guys.getUserName(),Guys);
        hashtable.put(Scam.getUserName(),Scam);

        hashtable.printHashTable();

        System.out.println(hashtable.get("Dong"));

        hashtable.remove("Dong");

        hashtable.printHashTable();



    }
}
