package com.company;

public class SimpleHashtable {
    private StoredPlayer[] hashTable;

    public SimpleHashtable(){
        hashTable = new StoredPlayer[10];
    }

    private int hashKey(String Key){
        return Key.length() % hashTable.length;
    }

    public void put(String key, Player value){
        int hashedkey = hashKey(key);

        if(isOccupied(hashedkey)){
            int stoppingIndex  = hashedkey;

            if(hashedkey == hashTable.length - 1){
                hashedkey = 0;
            }
            else{
                hashedkey++;
            }
            while(isOccupied(hashedkey) && hashedkey != stoppingIndex){
               hashedkey = (hashedkey + 1) % hashTable.length;
            }

        }

        if(hashTable[hashedkey] != null){
           System.out.println("THere is an element in the " + hashedkey + " position");
        }
        else{
            hashTable[hashedkey] = new StoredPlayer(key,value);
        }

    }

    public Player get(String key){
        int hashedKey = findKey(key);

        if(hashedKey == -1){
            return null;
        }
        return hashTable[hashedKey].value;
    }

    private int findKey(String key){
        int hashedKey = hashKey(key);

        if(hashTable[hashedKey] != null && hashTable [hashedKey].key.equals(key)){
            return   hashedKey;
        }

        int stoppingIndex = hashedKey;

        if (hashedKey == hashTable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }
        while (hashedKey != stoppingIndex
                && hashTable[hashedKey] != null
                && !hashTable[hashedKey].key.equals(key)) {
            hashedKey = (hashedKey + 1) % hashTable.length;
        }

        if(hashTable[hashedKey] != null && hashTable[hashedKey].key.equals(key)){
            return hashedKey;
        }
        return -1;

    }


    private boolean isOccupied(int index){
        return hashTable[index] != null;
    }

    public void printHashTable(){
        for(int i = 0; i < hashTable.length; i++){
            System.out.println("Element " + i + " :" + hashTable[i]);
        }
    }

    public SimpleHashtable remove(String stringGiven){
        int hashedKey = findKey(stringGiven);
        hashTable[hashedKey] =  null;
        return  null;


    }

}
